# Sample Lookup Service


## Project Conventions

Contributions to this project should follow the branching model described in this README.
Any additional documentation should be kept in Confluence at the page linked in the title.

### Summary

* `master` - always stable policy; requires approval
* `develop` - unstable staging for `master`
* `feature/*`, `bug/*` - branch off `develop`
* Release by tagging noteworthy versions of master

### Details

1.	Most mainstream work should be done on the `develop` branch.  It is
		not required to be stable at all times.
		
2.	When work comes to a stopping point and is considered stable, it should
		be merged to `master` via a merge request in stash, to be reviewed by
		the project owners.  `master` is required to be stable.
		
3.	When a version of `master` is to be considered usable in the field,
		that version should be tagged.
		*Example:* `v1.0.1`
		
4.	Only tagged versions should be considered usable in the field.  Release
		branches are not applicable to this project since we will be enforcing
		a one-size-fits-all model.  (Anything requiring customization should be
		forked to its own project rather than living here.)
		
5.	When it is appropriate to branch `develop` to work on a new feature
		on the side, a `feature` branch should be created.  Other purposes are
		also welcome (i.e., `bug/*` to fix a certain bug).
		*Example:* `feature/doodlebobbers`
		
6.	There are always exceptions.  Just keep it neat and get the work done.