# -*- coding: utf-8 -*-
"""
Fixture data for testing the lookup
"""

# Example of the data once it is retrieved from the service
data_source_records = [{'client': 'Gobo',
                        'keys': ['k001', 'k002'],
                        'data_source_server': 'server00',
                        'data_source_volume': 'db-gobo',
                        },
                       {'client': 'Mokey',
                        'keys': [],
                        'data_source_server': 'server00',
                        'data_source_volume': 'db-moke',
                        },
                       {'client': 'Wembley',
                        'keys': ['k003', 'k004', 'k005', 'k006'],
                        'data_source_server': 'server01',
                        'data_source_volume': 'db-wemb',
                        },
                       ]

# Example of how the service provides the data
sample_retrieval_result = '''
    <Result status="success">
       [{"keys": ["k001", "k002"], "client": "Gobo", "data_source_server": "server00", "data_source_volume": "db-gobo"},
        {"keys": [], "client": "Mokey", "data_source_server": "server00", "data_source_volume": "db-moke"},
        {"keys": ["k003", "k004", "k005", "k006"], "client": "Wembley", "data_source_server": "server01", "data_source_volume": "db-wemb"}
       ]
    </Result>'''.strip()
