# -*- coding: utf-8 -*-
"""
Provides look-up logic for the service
"""
from load_fixture_data import data_source_records, sample_retrieval_result

# TODO: Define paramters
def get_data_source()
    """
    Look up the data source server and volume based on the supplied session_key or client_id
    :param client_id: A client ID uniquely identifying a data source
    :param session_key: A session key that will be unique and will identify a data source
    :return: Tuple of strings (data_source_server, data_source_volume)
    """
    # TODO: Implement the logic
    pass


if __name__ == '__main__':
    # Test get_data_source by client
    data_source_server, data_source_volume = get_data_source(client_id='Gobo')
    assert data_source_server == 'server00'
    assert data_source_volume == 'db-gobo'
    print("Test by client passed")

    # Test get_data_source by client
    data_source_server, data_source_volume = get_data_source(session_key='k004')
    assert data_source_server == 'server01'
    assert data_source_volume == 'db-wemb'
    print("Test by session passed")
